#!/bin/bash

# add comment if you want to disable debugging
set -x

# setting var
username="juanmiguel.martin"
terraform_version="0.12.29"
packer_version="1.6.0"

# create new user
useradd $username -m -G wheel -c "User admin"

# wheel group sudos without password
sed -i 's/^\%wheel/\#\%wheel/g' /etc/sudoers
sed -i 's/^\# \%wheel/\%wheel/g' /etc/sudoers

# create new ssh key
mkdir -p /home/$username/.ssh \
&& ssh-keygen -t rsa -b 4096 -f /home/$username/.ssh/id_rsa_$username -N '' \
&& chown -R $username:$username /home/$username/.ssh

# install packages
yum -y update
yum install -y docker ansible unzip wget python3-pip git
yum clean all

# add docker privileges
groupadd docker
usermod -aG docker $username

# install aws cli
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
rm -rf ./aws
rm -f awscliv2.zip

# install terraform
wget -q https://releases.hashicorp.com/terraform/${terraform_version}/terraform_${terraform_version}_linux_amd64.zip \
&& unzip -o terraform_${terraform_version}_linux_amd64.zip -d /usr/local/bin \
&& rm terraform_${terraform_version}_linux_amd64.zip


# install packer
wget -q https://releases.hashicorp.com/packer/${packer_version}/packer_${packer_version}_linux_amd64.zip \
&& unzip -o packer_${packer_version}_linux_amd64.zip -d /usr/local/bin \
&& rm packer_${packer_version}_linux_amd64.zip

# configure vim

curl https://gitlab.com/conf1/vim/-/raw/master/.vimrc -o /home/$username/.vimrc
